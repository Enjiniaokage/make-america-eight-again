/* TODO:
 *	Get raspi to host on (Am I really that invested in such a stupid project?)
 *	Write script to restart bot whenever a change is made to repo
 */

let Twit = require('twit');

let credentials = {
  consumer_key: 'KNev7CqMrve7oiN3b3IjlVIXz',
  consumer_secret: 'nfsoC9Mm025fBd7yniL62CHimNz00URVwnNU4V5yrTQ9EOlKNh',
  access_token: '947971854169530369-UDeyscawob7yht71bOrIbnu45YjbnFG',
  access_token_secret: '9spVfvT5jEELtKfIJWeP4mQrb1pD4bjTJDzvk0n8fOYDi',
};

let T = new Twit(credentials);

let whiteList = [{
    find: [/left wing/gi, /democrat/gi, /liberal/gi, /hillary/gi, /obama/gi, /FBI/gi, /CIA/gi, /dem/gi],
    replace: ['doo doo head', 'pee pee head', 'booger eater', 'buttface', 'poopy head', 'stupid face', 'meanie', 'dingus', 'stupid', 'dumbo', 'loser', 'dummy', 'idiot']
  },
  {
    find: [/obamacare/gi],
    replace: ['oDUMacare', 'badcare']
  },
  {
    find: [/media/gi, /press/gi, /news/gi],
    replace: ['story', 'popup book', 'tale']
  },
  {
    find: [/conservative/gi, /republican/gi, /right wing/gi],
    replace: ['friend', 'koolkid', 'winner', 'smartie', 'best']
  },
  {
    find: [/tax cut/gi, /funding/gi],
    replace: ['allowance', 'pay']
  },
  {
    find: [/making america great again/gi, /make america great again/gi],
    replace: ['make america eight again']
  },
  {
    find: [/united states/gi, /oval office/gi, /country/gi],
    replace: ['clubhouse', 'schoolyard', 'yard', 'fort', 'club']
  },
  {
    find: [/bill/gi, /law/gi],
    replace: ['rule']
  },
  {
    find: [/kim jong un/gi],
    replace: ['rocketboy', 'rocketman']
  },
  {
    find: [/scientist/gi],
    replace: ['nerd', 'geek']
  },
  {
    find: [/journalist/gi],
    replace: ['liar', 'fake', 'bad people']
  },
  {
    find: [/president/gi],
    replace: ['leader']
  },
  {
    find: [/terrorist/gi, /shooter/gi, /bomber/gi, /enemy/gi],
    replace: ['buttface', 'bad man', 'meanie', 'bully']
  },
  {
    find: [/greater/gi, /great/gi, /best/gi],
    replace: ['goodest', 'bestest', 'gooder', 'bester']
  },
  {
    find: [/conference/gi, /interview/gi, /meeting/gi],
    replace: ['playdate', 'hangout']
  },
  {
    find: [/corrupt/gi, /broken/gi, /poor/gi],
    replace: ['stupid', 'dumb', 'bad']
  },
  {
    find: [/north korea/gi, /somalia/gi, /mexico/gi, /border/gi, /china/gi, /syria/gi, /lybia/gi, /sudan/gi, /yemen/gi, /iran/gi, /iraq/gi],
    replace: ['the bad place', 'dorktown', 'stupidton', 'tims']
  },
  {
    find: [/australia/gi, /germany/gi, /canada/gi, /israel/gi, /russia/gi, /france/gi, /japan/gi],
    replace: ['the good place', 'coolsville', 'alexs']
  },
  {
    find: [/pen /gi],
    replace: ['colored pencil ', 'crayon ', 'paint ']
  },
  {
    find: [/government/gi, /congress/gi, /senate/gi],
    replace: ['classroom', 'class', 'club']
  },
  {
    find: [/immigrant/gi],
    replace: ['transfer student', 'uckies']
  },
  {
    find: [/military/gi, /army/gi, /navy/gi, /soldiers/gi, /troops/gi],
    replace: ['baseball players', 'basketball players', 'rugby players', 'quarterbacks', 'kickers', 'teammates', 'athletes']
  },
  {
    find: [/shut down/gi, /shutdown/gi],
    replace: ['tantrum', 'hissy fit', 'fit']
  },
  {
    find: [/economic/gi, /wealth/gi],
    replace: ['richness', 'rich']
  },
  {
    find: [/prime minister/gi, /mayor/gi, /governer/gi, /senator/gi, /general/gi, /admiral/gi, /captain/gi],
    replace: ['important man', 'powerful man', 'strong man', 'big man']
  }
  // {
  // 	find: [],
  // 	replace: []
  // },
];

//Trump ID
const ID = '25073877';

//code start
console.log('Build that wall!');

const DELDIST = 20;
const CAPSDIST = 20;


let hashtags = [];

let stream = T.stream('statuses/filter', {
  follow: ID
});

stream.on('tweet', tweetCheck);

function tweetCheck(tweet) {
  if (tweet.user.id_str === ID) {
    hashtags = tweet.entities.hashtags;

    let text = (tweet.hasOwnProperty('extended_tweet')) ? tweet.extended_tweet.full_text : tweet.text
    let mentions = tweet.entities.user_mentions;
    // if a user is mentioned, remove their handle as to not notify them
    // can't use replace for this in the event that trump uses the @ symbol properly :c
    for (mention of mentions) {
      if (mention.id_str === ID) {
        text = text.swap(mention.indices[0] + 1, 15, 'ReALJoNALdPlUmP');
      }
      text = text.swap(mention.indices[0], 1, '_');
    }

    tweet = makeEight(text);

    T.post('statuses/update', {
      status: tweet
    }, function(err, data, response) {
      console.log('tweeted :' + tweet);
    })
  }
}

function makeEight(tweet) {
  //replace words with replacements
  for (subList of whiteList) {
    for (findPhrase of subList.find) {
      let findSize = findPhrase.toString().length - 4;
      let findIndices = tweet.regexIndexOfAll(findPhrase);
      if (findIndices.length != 0) {
        for (var i = findIndices.length - 1; i >= 0; i--) {

          let index = findIndices[i];

          let firstCapitalized = (tweet[index] === tweet[index].toUpperCase());
          let midCapitalized = (tweet[Math.floor(index + (findSize / 2))] === tweet[Math.floor(index + (findSize / 2))].toUpperCase());
          let wordCaps = (firstCapitalized && midCapitalized);
          let wordInitCaps = (firstCapitalized && !midCapitalized);

          let newPhrase = getResponse(subList.replace, tweet.length, findSize, wordCaps, wordInitCaps, index);

          tweet = tweet.swap(index, findSize, newPhrase);
        }
      }
    }
  }

  //randomly remove vowels
  let currentIndex = 0;
  while (currentIndex < tweet.length) {
    if (tweet[currentIndex].isVowel()) {
      tweet = tweet.swap(currentIndex, 1, "");
    }
    currentIndex += rand(DELDIST);
  }

  //randomly change capitalization
  currentIndex = 0;
  while (currentIndex < tweet.length) {
    tweet = tweet.invertCase(currentIndex);
    currentIndex += rand(CAPSDIST);
  }


  return tweet;
}

function getResponse(list, size, findSize, capsLock, initCaps, newIndex) {
  let ind = rand(list.length);
  while (ind >= 0) {
    let newPhrase = list[ind];
    if (size - findSize + newPhrase.length < 280) {
      for (hashtag of hashtags) {
        if (hashtag.indices[0] < newIndex && hashtag.indices[1] > newIndex) {
          newPhrase = newPhrase.replace(/\s/g, '_');
        }
      }
      if (capsLock) {
        newPhrase = newPhrase.toUpperCase();
      }
      if (initCaps) {
        newPhrase = newPhrase.swap(0, 1, newPhrase[0].toUpperCase());
      }
      return newPhrase;
    } else {
      ind--;
    }
  }
  crash('Ran out of hot air');
}

function crash(text) {
  T.post("direct_messages/new", {
    user_id: '2434859912',
    text: 'PlumpBot has crashed. Err:' + text
  });
  throw text;
}

//prototype functions for string manipulation
String.prototype.regexIndexOfAll = function(regex) {
  let lastIndex = 0;
  let newIndex = 0;
  let findIndices = [];
  while (newIndex != -1) {
    let subString = this.substring(lastIndex);
    newIndex = subString.search(regex);
    if (newIndex != -1) {
      findIndices.push(newIndex + lastIndex);
    }
    lastIndex += (newIndex + 1);
  }
  return findIndices;
}

String.prototype.swap = function(startIndex, findSize, newWord) {
  return this.slice(0, startIndex) + newWord + this.slice(startIndex + findSize, this.length);
}

String.prototype.isVowel = function() {
  if (this.length == 1) {
    return /[aeiouAEIOU]/.test(this);
  } else {
    return false;
  }
}

String.prototype.invertCase = function(index) {
  return this.swap(index, 1, this[index].toUpperCase());
}

//extra functions for ease of use

function rand(topBound) {
  return Math.floor((Math.random() * (topBound)));
}